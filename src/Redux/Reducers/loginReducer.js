const initialState={
    token: 0
};
   export const loginReducer=(state=initialState, action)=>{
    switch(action.type){
        case 'LOGIN':
            return { ...state, token: action.payload};
        default:
            return state; 
    }
}