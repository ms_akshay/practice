import {loginReducer} from '../Redux/Reducers/loginReducer';
import { createStore} from 'redux';

export default createStore(loginReducer);