import React, { useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen'
import {connect} from 'react-redux';
import { AppNavigator, AuthNavigator } from './routes';

const RootNavigator = (props) => {
    useEffect(() => {
        SplashScreen.hide();
    },[])
    
    return (
        <>
              {     props.token ? (
                <AppNavigator/>
            ) : <AuthNavigator/>
        }
        </>
    )
}

const mapStateToProps=state=>{
    return {
        token: state.token,
    };
}

export default connect(mapStateToProps)(RootNavigator);
