import React from "react";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Home, Search, Settings, Notifications} from '../screens';


const HomeTabs = () => {
    const Tab = createBottomTabNavigator();
    return (
        <Tab.Navigator  
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName;
                
                if (route.name === 'Home') {
                    iconName = 'home';
                }
                if (route.name === 'Search'){
                    iconName = 'search';
                }
                if(route.name === 'Notifications') {
                    iconName='notifications'
                }
                if (route.name === 'Settings'){
                    iconName = 'settings';
                }
                
                return <Ionicons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: 'tomato',
                tabBarInactiveTintColor: 'white',
                headerShown: false,
                tabBarStyle:{borderTopLeftRadius:20, borderTopRightRadius:20,backgroundColor:'blue', height: 70, paddingBottom: 6},
                tabBarLabelStyle:{paddingBottom:1},
          })}
          
          >
            <Tab.Screen name="Home" component={Home} />
            <Tab.Screen name="Search" component={Search} />
            <Tab.Screen name="Notifications" component={Notifications} />
            <Tab.Screen name="Settings" component={Settings} />
      </Tab.Navigator>
    );
}

export default HomeTabs;