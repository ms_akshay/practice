import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeTabs from './HomeTabs';

const AppNavigator = () => {
    const Stack = createNativeStackNavigator();

    return (
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
          >
            <Stack.Screen name="HomeTabs" component={HomeTabs} />
          </Stack.Navigator>
        </NavigationContainer>
      );
}

export default AppNavigator;