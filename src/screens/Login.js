import React, {useState, useEffect} from 'react';
import {Text,SafeAreaView, View, StyleSheet} from 'react-native';
import { s, vs} from 'react-native-size-matters';
import {connect} from 'react-redux';

import {login} from '../Redux/Actions/loginActions';
import { Button, Input } from '../Components';
import { validator } from '../utils/FormValidators';

const Login = (props) => {
    const {navigation} = props;
    const [state, setState] = useState({
        email : '',
        password: '',
        disable: true,
        emailError : '',
        passwordError : '',
    });

    const {email, password, disable, emailError,passwordError} = state;

    const handleValidate = async(value, key) => {
        let isValid = await validator(value, key);
        setState((prevState) => {
         return {
             ...prevState,
             [key] : value,
             [`${key}Error`] : isValid
         }
        })
     };

     useEffect(() => {
       if(emailError && passwordError) {
        setState({
            ...state,
            disable: false
        })
       }
       else {
        setState({
            ...state,
            disable: true
        })
       }
     }, [emailError, passwordError])

     const handleLogin  = () => {
       props.login(1234)
     }

    return (
        <SafeAreaView style={{flex: 1}}>
            <View style={styles.container}>
                <Text style={styles.headerText}>Login</Text>
                <Input
                    placeholder='email'
                    value={email}
                    name={'email'}
                    onChange={(value, key) =>  handleValidate(value, key)}

                />
                {
                    email && !emailError && (
                        <Text style = {styles.error}>Invalid Email</Text>
                    )
                }
               
                <Input
                    placeholder='password'
                    value={password}
                    name={'password'}
                    onChange={(value, key) =>  handleValidate(value, key)}
                    isSecure={true}
                />
                
                {
                    password && !passwordError && (
                        <Text style = {styles.error}>Invalid password</Text>
                    )
                }

                <Button
                    title={'Log In'}
                    onPress={() => handleLogin()}
                    isDisable={disable}
                />
                <View style={styles.footerContainer}>
                    <Text style={styles.bottomText}>
                        {"Don't have an account ? "} 
                    </Text>
                    <Text style={[styles.bottomText, styles.signupText]}  onPress={() => navigation.navigate('Signup')}>
                        Signup
                    </Text>
                </View>
            </View>
        </SafeAreaView>
    );
}



const mapDispatchToProps = dispatch => {
	return{
        login : (data) => {
            dispatch(login(data));
        },
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: vs(100),
        alignItems: 'center',
        paddingHorizontal: s(20),
    },
    headerText: {
        color: 'black',
        fontSize: s(20),
        marginBottom: vs(30),
        fontWeight: '700'
    },
  
    loginButton: {
        backgroundColor: 'blue',
        width: '100%',
        borderRadius: s(8),
        paddingVertical: vs(12),
        marginTop: vs(30),
    },
    loginText: {
        color: 'white',
        fontSize: s(13),
        alignSelf: 'center',
        fontWeight: '500'
    },
    footerContainer: {
        flexDirection: 'row',
        marginTop: vs(24),
    },
    signupText: {
        fontWeight: '500',
        color: 'blue'
    },
    bottomText: {
        color: 'black',
        fontSize: s(14),
    },
    error : {
        color: 'red',
        alignSelf: 'flex-start',
        marginBottom: vs(6)
    }
  });

export default connect(null, mapDispatchToProps)(Login);