import React from "react";
import {Text, SafeAreaView, StyleSheet, View} from 'react-native';


const Home  = () => {
    return (
        <SafeAreaView style={{flex: 1}}>
            <View style = {styles.container}>
                <Text>
                    Home...
                </Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
    },  
})

export default Home;