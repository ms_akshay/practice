import React, {useState, useCallback, useEffect} from 'react';
import {Text,SafeAreaView, StatusBar, View, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import { s, vs} from 'react-native-size-matters';

import { validator } from '../utils/FormValidators';
import { Button, Input } from '../Components';

const initialState = {
    userName : '',
    email : '',
    emailError : '',
    passwordError: '',
    password: '',
    disable: true,
}

const Signup = ({navigation}) => {
    const [state, setState] = useState({...initialState});
    const {userName,emailError, passwordError, email, password, disable} = state;

    const handleChange = async(value, key) => {
        let isValid = await validator(value, key);
        setState((prevState) => {
        return {
            ...prevState,
            [key] : value,
            [`${key}Error`] : isValid
        }
       })
    }

    useEffect(() => {
        if(userName && emailError && passwordError) {
         setState({
             ...state,
             disable: false
         })
        }
        else {
         setState({
             ...state,
             disable: true
         })
        }
      }, [emailError, passwordError])

    return (
        <SafeAreaView style = {{flex: 1}}>
                <View style={styles.container}>
                    <Text style={styles.headerText}>Signup</Text>
                    
                    <Input
                      value={userName}
                      placeholder='userName'
                      name='userName'
                      onChange={(value, key) =>  handleChange(value, key)}
                    />

                    <Input
                      value={email}
                      placeholder='email'
                      name='email'
                      onChange={(value, key) =>  handleChange(value, key)}
                    />
                    
                    {
                    email && !emailError && (
                        <Text style = {styles.error}>Invalid Email</Text>
                        )
                    }

                    <Input
                      value={password}
                      placeholder='password'
                      name='password'
                      onChange={(value, key) =>  handleChange(value, key)}
                      isSecure={true}
                    />

                    {
                    password && !passwordError && (
                        <Text style = {styles.error}>Invalid password</Text>
                        )
                    }
                  
                    <Button
                        title = {'Sign up'}
                        onPress={() => {console.log("GGg")}}
                        isDisable={disable}
                    />
                    <View style={styles.footerContainer}>
                        <Text style={styles.bottomText}>
                            {"Have an account ? "} 
                        </Text>
                        <Text style={[styles.bottomText, styles.signupText]}  onPress={() => navigation.navigate('Login')}>
                            Login
                        </Text>
                    </View>
                </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: vs(100),
        alignItems: 'center',
        paddingHorizontal: s(20),
    },
    headerText: {
        color: 'black',
        fontSize: s(20),
        marginBottom: vs(30),
        fontWeight: '700'
    },
    footerContainer: {
        flexDirection: 'row',
        marginTop: vs(24),
    },
    signupText: {
        fontWeight: '500',
        color: 'blue'
    },
    bottomText: {
        color: 'black',
        fontSize: s(14),
    },
    error : {
        color: 'red',
        alignSelf: 'flex-start',
        marginBottom: vs(6)
    }
  });

export default Signup;