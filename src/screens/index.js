import Home from './Home';
import Login from './Login';
import Signup from './Signup';
import Search from './Search';
import Notifications from './Notifications';
import Settings from './Settings';

export {
    Home,
    Login,
    Signup,
    Search,
    Notifications,
    Settings
}