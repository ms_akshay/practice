import React, { useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen'
import {Provider} from 'react-redux';
import store from './Redux/store';
import RootNavigator from './RootNavigator';

const App = () => {
    useEffect(() => {
        SplashScreen.hide();
    },[])
    
    return (
        <Provider store={store}>
            <RootNavigator/>
        </Provider>
    )
}


export default App;
