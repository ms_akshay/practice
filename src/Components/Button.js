import React from "react";
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import { s, vs, ms, mvs } from 'react-native-size-matters';


const Button = ({
    title = '',
    onPress = () => {},
    isDisable = false,
}) => {
    return (
        <>
            <TouchableOpacity style={[styles.button, isDisable && styles.disableButton]} activeOpacity={0.7} onPress={onPress} disabled={isDisable}> 
                <Text style={styles.buttonText}>
                    {title}
                </Text>
            </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'blue',
        width: '100%',
        borderRadius: s(8),
        paddingVertical: vs(12),
        marginTop: vs(30),
    },
    buttonText: {
        color: 'white',
        fontSize: s(13),
        alignSelf: 'center',
        fontWeight: '500'
    },
    disableButton : {
        backgroundColor: 'grey'
    }
})

export default Button