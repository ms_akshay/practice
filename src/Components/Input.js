import React from "react";
import {StyleSheet, TextInput} from 'react-native';

import { s, vs} from 'react-native-size-matters';

const Input = ({
    placeholder = '',
    value = '',
    name = '',
    onChange = () => {},
    isSecure= false,
}) => {
    return (
       <TextInput
        placeholder={placeholder}
        value={value}
        onChangeText={(value) =>  onChange(value , name)}
        style={styles.input}
        secureTextEntry={isSecure}
       />
    )
}

const styles = StyleSheet.create({
    input: {
        borderColor: 'black',
        borderWidth: s(1),
        width: '100%',
        paddingHorizontal: s(10),
        paddingVertical: vs(12),
        margin: s(10),
        borderRadius: s(5)
    },
})

export default Input;